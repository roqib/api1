<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class biodata extends Model
{
    protected $table = 'biodatas';
    protected $fillable = [
        'nama','alamat','nama_ayah','nama_ibu','anak_ke'
    ];
}
